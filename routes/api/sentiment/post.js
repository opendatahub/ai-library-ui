const urlJoin = require("url-join");
const Wreck = require("wreck");


handler = async (request, h) => {
  const apiRequestUrl = urlJoin(request.server.app.aiLibrary.url, "/sentiment-analysis");
  const headers = {Authorization: request.server.app.aiLibrary.basicAuthHeader, ...request.headers};

  delete headers.host;
  delete headers["content-length"];

  try {
    const res = await Wreck.request(
      "POST",
      apiRequestUrl,
      {
        payload: {
          "sentimentFields": "text",
          "discardData": true,
          "type": "general",
          "data": {
            "text": request.payload.text
          }
        },
        headers: headers,
        rejectUnauthorized: false
      });

    const r = await Wreck.read(res, {json: true});
    if (res.statusCode > 299) {
      console.error(request);
      return h.response(r).code(res.statusCode);
    }

    return h.response({
      metadata: {
        type: "Sentiment",
        async: {
          status: "in_progress"
        }
      },
      data: {
        id: r.activationId
      }
    }).code(202);
  }
  catch (err) {
    console.error(err);
    return h.response(err).code(500);
  }
};

module.exports.handler = handler;
