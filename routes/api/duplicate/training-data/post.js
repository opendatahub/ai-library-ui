const uuid = require("uuid");
const Joi = require("joi");
const servicePrefix = require("../constants").servicePrefix;
const dataSetPrefix = require("../constants").trainingDataPrefix;
const storageErrorResponseBody = require("../../utilities").storageErrorResponseBody;


handler = async (request, h) => {
  const storage = request.server.app.aiLibrary.storage;
  const id = uuid.v4();
  const fileKey = `${servicePrefix}/${dataSetPrefix}/${id}/duplicate-training-data.json`;
  const dateStr = new Date().toISOString();
  const obj = {
    ...request.payload,
    id,
    createdAt: dateStr,
    modifiedAt: dateStr
  };
  try {
    await storage.writeJson(fileKey, obj);
    return h.response({
      metadata: {
        type: "DuplicateTrainingData",
        async: {
          status: "success"
        }
      },
      data: obj
    }).code(201);
  }
  catch (err) {
    console.error(err);
    let responseBody = storageErrorResponseBody(err);
    return h.response(responseBody).code(responseBody.statusCode);
  }
};

module.exports.handler = handler;
