addTrainingDataRoutes = (server) => {
  require("./records")(server);

  server.route({
    method: ["GET"],
    path: "/api/duplicate/training-data/{id}",
    handler: require("./get").handler
  });


  server.route({
    method: ["POST"],
    path: "/api/duplicate/training-data",
    handler: require("./post").handler,
  });
};

module.exports = addTrainingDataRoutes;