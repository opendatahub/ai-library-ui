const servicePrefix = require("../../constants").servicePrefix;
const dataSetPrefix = require("../../constants").trainingDataPrefix;
const trainingDataRecordsPrefix = require("../../constants").trainingDataRecordsPrefix;
const storageErrorResponseBody = require("../../../utilities").storageErrorResponseBody;

handler = async (request, h) => {
  const storage = request.server.app.aiLibrary.storage;
  const id = request.params.id;
  const fileKey = `${servicePrefix}/${dataSetPrefix}/${request.params.setId}/${trainingDataRecordsPrefix}/${id}.json`;

  try {
    let obj = await storage.readJson(fileKey);
    return h.response({
      metadata: {
        type: "DuplicateTrainingDataRecord",
        async: {
          status: "success"
        }
      },
      data: obj
    }).code(200);
  }
  catch (err) {
    console.error(err);
    let responseBody = storageErrorResponseBody(err);
    return h.response(responseBody).code(responseBody.statusCode);
  }
};

module.exports.handler = handler;