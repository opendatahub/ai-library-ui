const urlJoin = require("url-join");
const Wreck = require("wreck");
const Joi = require("joi");
const uuid = require("uuid");

const servicePrefix = require("../constants").servicePrefix;
const trainingDataPrefix = require("../constants").trainingDataPrefix;
const trainingDataRecordsPrefix = require("../constants").trainingDataRecordsPrefix;
const modelsPrefix = require("../constants").modelsPrefix;
const storageErrorResponseBody = require("../../utilities").storageErrorResponseBody;

handler = async (request, h) => {
  const storage = request.server.app.aiLibrary.storage;

  const id = uuid.v4();
  const trainingDataId = request.payload.trainingDataId;

  const dateStr = new Date().toISOString();
  const modelData = {
    id,
    trainingDataId,
    createdAt: dateStr,
    modifiedAt: dateStr
  };

  const modelDestination = `${storage.s3Prefix}/${servicePrefix}/${modelsPrefix}/${id}/${trainingDataRecordsPrefix}`;
  const trainingDataSource = `${storage.s3Prefix}/${servicePrefix}/${trainingDataPrefix}/${trainingDataId}/${trainingDataRecordsPrefix}`;
  const app_args = `-s3Path=${trainingDataSource} -s3Destination=${modelDestination}`;



  //properly clean and write metadata about model
  try {
    const modelJsonDir = `${servicePrefix}/${modelsPrefix}/${id}`;
    await storage.deleteFile(modelJsonDir);

    const modelJsonDestination = `${modelJsonDir}/duplicate-model.json`;
    await storage.writeJson(modelJsonDestination, modelData);
  }
  catch (err) {
    console.error(err);
    let responseBody = storageErrorResponseBody(err);
    return h.response(responseBody).code(responseBody.statusCode);
  }

  // run the job to create model
  const apiRequestUrl = urlJoin(request.server.app.aiLibrary.url, "/duplicate-bug-detection-training");
  let headers = {Authorization: request.server.app.aiLibrary.basicAuthHeader, ...request.headers};
  delete headers.host;
  delete headers["content-length"];

  try {
    const res = await Wreck.request(
      "POST",
      apiRequestUrl,
      {
        payload: { name: id, app_args },
        headers: headers,
        rejectUnauthorized: false
      });

    const r = await Wreck.read(res, {json: true});
    if (res.statusCode > 299) {
      console.error(request);
      return h.response(r).code(res.statusCode);
    }

    return h.response({
      metadata: {
        type: "DuplicateModel",
        async: {
          status: "in_progress",
        }
      },
      data: modelData
    }).code(202);
  }
  catch (err) {
    console.error(err);
    return h.response(err).code(500);
  }
};

validate = {
  payload: {
    trainingDataId: Joi.string().required()
  }
};

module.exports.handler = handler;
module.exports.validate = validate;
