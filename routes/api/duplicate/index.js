

addDuplicateRoutes = (server) => {
  require("./training-data")(server);
  require("./models")(server);
  require("./prediction-data")(server);
  require("./predictions")(server);
};

module.exports = addDuplicateRoutes;