const servicePrefix = require("../../constants").servicePrefix;
const predictionsPrefix = require("../../constants").predictionsPrefix;
const predictionRecordsPrefix = require("../../constants").predictionRecordsPrefix;
const storageErrorResponseBody = require("../../../utilities").storageErrorResponseBody;

handler = async (request, h) => {
  const storage = request.server.app.aiLibrary.storage;
  const {id, predictionId} = request.params;
  const filePath = `${servicePrefix}/${predictionsPrefix}/${predictionId}/${predictionRecordsPrefix}/${id}.json`;

  try {
    let obj = await storage.readJson(filePath);
    return h.response({
      metadata: {
        type: "DuplicatePredictionRecord",
        async: {
          status: "success"
        }
      },
      data: obj
    }).code(200);
  }
  catch (err) {
    console.error(err);
    let responseBody = storageErrorResponseBody(err);
    return h.response(responseBody).code(responseBody.statusCode);
  }
};

module.exports.handler = handler;