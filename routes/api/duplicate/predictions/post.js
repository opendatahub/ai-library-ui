const urlJoin = require("url-join");
const Wreck = require("wreck");
const Joi = require("joi");
const uuid = require("uuid");

const servicePrefix = require("../constants").servicePrefix;
const predictionsPrefix = require("../constants").predictionsPrefix;
const predictionRecordsPrefix = require("../constants").predictionRecordsPrefix;
const predictionDataPrefix = require("../constants").predictionDataPrefix;
const predictionDataRecordsPrefix = require("../constants").predictionDataRecordsPrefix;
const modelsPrefix = require("../constants").modelsPrefix;
const trainingDataRecordsPrefix = require("../constants").trainingDataRecordsPrefix;
const storageErrorResponseBody = require("../../utilities").storageErrorResponseBody;

handler = async (request, h) => {
  const storage = request.server.app.aiLibrary.storage;

  const id = uuid.v4();
  const modelId = request.payload.modelId;
  const predictionDataId = request.payload.predictionDataId;

  const predictionDestination = `${servicePrefix}/${predictionsPrefix}/${id}`;
  const dataJsonObj = {id, modelId, predictionDataId};
  const dataJsonDestination = `${predictionDestination}/prediction.json`;

  const modelLocation = `${storage.s3Prefix}/${servicePrefix}/${modelsPrefix}/${modelId}/${trainingDataRecordsPrefix}`;
  const dataSource = `${storage.s3Prefix}/${servicePrefix}/${predictionDataPrefix}/${predictionDataId}/${predictionDataRecordsPrefix}`;
  const predictionOutputDest = `${storage.s3Prefix}/${predictionDestination}/${predictionRecordsPrefix}`;
  const app_args = `-s3Path=${modelLocation} -bugs=${dataSource} -s3Destination=${predictionOutputDest}`;

  //properly clean and write metadata about model
  try {
    await storage.deleteDir(predictionDestination);
    await storage.writeJson(dataJsonDestination, dataJsonObj);
  }
  catch (err) {
    console.error(err);
    let responseBody = storageErrorResponseBody(err);
    return h.response(responseBody).code(responseBody.statusCode);
  }

  // run the job to create the predictions
  const apiRequestUrl = urlJoin(request.server.app.aiLibrary.url, "/duplicate-bug-detection-prediction");
  let headers = {Authorization: request.server.app.aiLibrary.basicAuthHeader, ...request.headers};
  delete headers.host;
  delete headers["content-length"];

  try {
    const res = await Wreck.request(
      "POST",
      apiRequestUrl,
      {
        payload: { name: id, app_args },
        headers: headers,
        rejectUnauthorized: false
      });

    const r = await Wreck.read(res, {json: true});
    if (res.statusCode > 299) {
      console.error(request);
      return h.response(r).code(res.statusCode);
    }

    return h.response({
      metadata: {
        type: "DuplicatePrediction",
        async: {
          status: "in_progress",
        }
      },
      data: dataJsonObj
    }).code(202);
  }
  catch (err) {
    console.error(err);
    return h.response(err).code(500);
  }
};

validate = {
  payload: {
    modelId: Joi.string().required(),
    predictionDataId: Joi.string().required()
  }
};

module.exports.handler = handler;
