const uuid = require("uuid");
const Joi = require("joi");
const servicePrefix = require("../../constants").servicePrefix;
const dataSetPrefix = require("../../constants").trainingDataPrefix;
const trainingDataRecordsPrefix = require("../../constants").trainingDataRecordsPrefix;
const storageErrorResponseBody = require("../../../utilities").storageErrorResponseBody;

handler = async (request, h) => {
  const storage = request.server.app.aiLibrary.storage;
  const id = uuid.v4();
  let {setId} = request.params;
  const fileKey = `${servicePrefix}/${dataSetPrefix}/${setId}/${trainingDataRecordsPrefix}/${id}.json`;
  const dateStr = new Date().toISOString();
  const obj = {
    ...request.payload,
    id,
    merged: request.payload.flake,
    createdAt: dateStr,
    modifiedAt: dateStr
  };

  if (setId === "sample") {
    return h.response({
      statusCode: 401,
      error: "Forbidden",
      message: "Forbidden:  Cannot modify sample data"
    }).code(401);
  }


  try {
    await storage.writeJson(fileKey, obj);
    return h.response({
      metadata: {
        type: "FlakeTrainingDataRecord",
        async: {
          status: "success"
        }
      },
      data: obj
    }).code(201);
  }
  catch (err) {
    console.error(err);
    let responseBody = storageErrorResponseBody(err);
    return h.response(responseBody).code(responseBody.statusCode);
  }
};

validate = {
  payload: {
    status: Joi.string().required(),
    label: Joi.string(),
    log: Joi.string().required(),
    flake: Joi.boolean().required()
  }
};

module.exports.handler = handler;
module.exports.validate = validate;

