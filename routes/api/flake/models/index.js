addTrainedModelRoutes = (server) => {
  server.route({
    method: ["GET"],
    path: "/api/flake/models/{id}",
    handler: require("./get").handler
  });

  server.route({
    method: ["POST"],
    path: "/api/flake/models",
    handler: require("./post").handler,
    options: {
      validate: require("./post").validate
    }
  });
};

module.exports = addTrainedModelRoutes;