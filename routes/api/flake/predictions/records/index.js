addPredictionRecordRoutes = (server) => {
  server.route({
    method: ["GET"],
    path: "/api/flake/predictions/{predictionId}/records/{id}",
    handler: require("./get").handler
  });

  server.route({
    method: ["GET"],
    path: "/api/flake/predictions/{predictionId}/records",
    handler: require("./list").handler
  });
};

module.exports = addPredictionRecordRoutes;