#!/bin/bash
set -x

source secrets

oc process -f openshift/ai-library-ui.yml \
  -p BASE_URL=${BASE_URL} \
  -p AI_LIBRARY_URL=${AI_LIBRARY_URL} \
  -p AI_LIBRARY_USERNAME=${AI_LIBRARY_USERNAME} \
  -p AI_LIBRARY_PASSWORD=${AI_LIBRARY_PASSWORD} \
  -p ACTIVATIONS_URL=${ACTIVATIONS_URL} \
  -p S3_ENDPOINT=${S3_ENDPOINT} \
  -p S3_BUCKET=${S3_BUCKET} \
  -p S3_PREFIX=${S3_PREFIX} \
  -p S3_ACCESS_KEY_ID=${S3_ACCESS_KEY_ID} \
  -p S3_SECRET_ACCESS_KEY=${S3_SECRET_ACCESS_KEY} \
  -p GIT_REF=${GIT_REF} \
  | oc create -f -
