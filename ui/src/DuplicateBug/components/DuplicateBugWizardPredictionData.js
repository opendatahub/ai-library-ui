import React from "react";
import { connect } from "react-redux";

import { Grid, Tab, Tabs } from "patternfly-react";

import JsonSample from "../../JsonSample/components/JsonSample";
import CodeSample from "../../CodeSample/components/CodeSample";
import generateCurl from "../../Utilities/generateCurl"
import { predictionData } from "./sampleData";
import DataTableGrid from "./DataTableGrid";


function generatePredictionDataCurlCommands(baseUrl) {
  const createdAt = new Date().toISOString();
  const modifiedAt = createdAt;
  let sampleCurlCommands = {};

  sampleCurlCommands.createPredictionDataCurl = generateCurl(
    'POST',
    baseUrl,
    "api/duplicate/prediction-data");

  sampleCurlCommands.createPredictionDataCurlResponse = {
    metadata: {
      type: "DuplicateBugPredictionData",
      async: {
        status: "success"
      }
    },
    data: {
      id: predictionData.id,
      createdAt,
      modifiedAt
    }
  };

  let trBody = {
    title: "Bug report title",
    content: "Bug report main content"
  };

  sampleCurlCommands.createPredictionDataRecordCurl = generateCurl(
    'POST',
    baseUrl,
    `api/duplicate/prediction-data/${predictionData.id}/records`,
    trBody);

  trBody = {
    id: predictionData.records[0].id,
    ...trBody,
    createdAt,
    modifiedAt
  };

  sampleCurlCommands.createPredictionDataRecordCurlResponse = {
    metadata: {
      type: "DuplicateBugPredictionDataRecord",
      async: {
        status: "success"
      }
    },
    data: trBody
  };

  return sampleCurlCommands;
}

class DuplicateBugWizardPredictionData extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      tabActiveKey: 0,
      curlCommands: generatePredictionDataCurlCommands(props.status.baseUrl)
    }
  }

  selectTab = (tabActiveKey) => {
    this.setState({tabActiveKey});
  };

  render() {
    const {tabActiveKey, curlCommands} = this.state;

    return (
      <div className="duplicate-bug-wizard-contents duplicate-bug-sample">
        <Tabs
          activeKey={tabActiveKey}
          onSelect={this.selectTab}
          animation={false}
          id="duplicateBugResultsTabs"
        >
          <Tab eventKey={0} title="Data">
            <div className="duplicate-bug-info-section">
              <h2>Prediction Data</h2>
              <p></p>
              <JsonSample title="Prediction Data Record (1 of 4)" object={{
                id: "uuid-generated-on-creation",
                title: "Bug report title",
                content: "Bug report main content"
              }}/>
              <h3>Demo Prediction Data</h3>
              <p>As part of this demonstration, we created 4 new data records to analyze.</p>
              <DataTableGrid data={predictionData.records}/>
            </div>
          </Tab>
          <Tab eventKey={1} title="API">
            <div className="duplicate-bug-info-section">
              <h2>Using the API - Creating Prediction Data Sets and Records</h2>
              <p>To create a prediction, we first to create the data set to process. Like training data, first create
                the prediction data set and then add records. We'll then use the prediction data set when running our
                prediction.</p>
              <h3>Create a Prediction Data Set</h3>
              <p>As part of this demonstration, we created a new data set.</p>
              <Grid.Row>
                <Grid.Col className="sample-request-col" lg={6}>
                  <CodeSample title="Request" code={curlCommands.createPredictionDataCurl} language="bash"/>
                </Grid.Col>
                <Grid.Col className="sample-response-col" lg={6}>
                  <JsonSample title="Response" object={curlCommands.createPredictionDataCurlResponse}/>
                </Grid.Col>
              </Grid.Row>
              <h3>Create a Prediction Data Record</h3>
              <p>We then added 10 prediction data records as part of that data set.</p>
              <Grid.Row>
                <Grid.Col className="sample-request-col" lg={6}>
                  <CodeSample title="Request (1 of 4)" code={curlCommands.createPredictionDataRecordCurl} language="bash"/>
                </Grid.Col>
                <Grid.Col className="sample-response-col" lg={6}>
                  <JsonSample title="Response (1 of 4)" object={curlCommands.createPredictionDataRecordCurlResponse}/>
                </Grid.Col>
              </Grid.Row>
            </div>
          </Tab>
        </Tabs>
      </div>
    );
  };
}

function mapStateToProps(state) {
  return {
    ...state.duplicateBugReducer,
    ...state.statusReducer
  };
}

export default connect(mapStateToProps)(DuplicateBugWizardPredictionData);
