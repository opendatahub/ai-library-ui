import React from "react";
import { connect } from "react-redux";
import {
  Wizard
} from "patternfly-react";

import scrollToSelector from "../../Utilities/scrollToSelector";
import ServiceTile from "../../ServiceTile/components/ServiceTile"

import DuplicateBugWizardInfo from "./DuplicateBugWizardInfo";
import DuplicateBugWizardTrainingData from "./DuplicateBugWizardTrainingData";
import DuplicateBugWizardTraining from "./DuplicateBugWizardTraining";
import DuplicateBugWizardPredictionData from "./DuplicateBugWizardPredictionData";
import DuplicateBugWizardPrediction from "./DuplicateBugWizardPrediction";
import DuplicateBugWizardDemo from "./DuplicateBugWizardDemo";

import { createDuplicatePrediction, getDuplicatePrediction, resetDuplicatePrediction } from "../actions";

import "./DuplicateBugCard.css"

class DuplicateBugCard extends React.Component {
  state = {
    showModal: false,
    showLoading: false,
    activeStepIndex: 0
  };

  cardClick = e => {
    e.preventDefault();
    this.openWizard();
  };

  openWizard = () => {
    this.setState({
      showModal: true,
      activeStepIndex: 0
    });
    this.props.resetDuplicatePrediction();
  };

  closeWizard = () => {
    this.setState({showModal: false});
    this.props.resetDuplicatePrediction();
  };

  disableWizardNext = () => {
    return false;
  };

  scrollToTop = () => {
    // hack to make sure we start from the top of each wizard
    scrollToSelector(".wizard-pf-main", 0, 0);
  };

  onWizardStepChange = newStepIndex => {
    switch (newStepIndex) {
      default:
        this.setState({activeStepIndex: newStepIndex}, this.scrollToTop);
        break;
    }
  };

  render() {
    const {showModal, activeStepIndex} = this.state;
    const wizardNextDisabled = this.disableWizardNext();
    const wizardSteps = [
      {
        title: "Introduction", render: () => (
          <DuplicateBugWizardInfo/>
        )
      },
      {
        title: "Training Data", render: () => (
          <DuplicateBugWizardTrainingData/>
        )
      },
      {
        title: "Training a Model", render: () => (
          <DuplicateBugWizardTraining/>
        )
      },
      {
        title: "Prediction Data", render: () => (
          <DuplicateBugWizardPredictionData/>
        )
      },
      {
        title: "Predictions", render: () => (
          <DuplicateBugWizardPrediction/>
        )
      },
      {
        title: "Demo", render: () => (
          <DuplicateBugWizardDemo/>
        )
      }
    ];

    return (
      <div>
        <ServiceTile
          key="tile-duplicate"
          title="Duplicate Bug Detection"
          featured={true}
          iconClass="fa fa-bug"
          description="Identify duplicate bug reports"
          onClick={this.cardClick}
        />
        <Wizard.Pattern
          className="duplicate-bug-wizard service-wizard"
          show={showModal}
          backdrop="static"
          onHide={this.closeWizard}
          onExited={this.closeWizard}
          title={<span className="modal-title"><span className="service-wizard-title-icon fa fa-bug"/> Duplicate Bug Detection</span>}
          nextStepDisabled={wizardNextDisabled}
          steps={wizardSteps}
          onStepChanged={this.onWizardStepChange}
          loading={false}
          activeStepIndex={activeStepIndex}
        />
      </div>
    );
  };
}

function mapStateToProps(state) {
  return {
    ...state.duplicateBugReducer,
    ...state.statusReducer
  };
}

function mapDispatchToProps(dispatch) {
  return {
    createDuplicatePrediction: (bugs, modelId) => {
      dispatch(createDuplicatePrediction(bugs, modelId));
    },
    getDuplicatePrediction: (id) => {
      dispatch(getDuplicatePrediction(id));
    },
    resetDuplicatePrediction: () => {
      dispatch(resetDuplicatePrediction());
    }
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(DuplicateBugCard);
