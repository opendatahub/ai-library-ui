import React from "react";
import { connect } from "react-redux";

import { Grid, Tab, Tabs } from "patternfly-react";

import JsonSample from "../../JsonSample/components/JsonSample";
import CodeSample from "../../CodeSample/components/CodeSample";
import generateCurl from "../../Utilities/generateCurl"
import { prediction } from "./sampleData";


function generatePredictionCurlCommands(baseUrl) {
  let sampleCurlCommands = {};

  sampleCurlCommands.createPredictionCurl = generateCurl(
    'POST',
    baseUrl,
    "api/duplicate/predictions",
    {modelId:prediction.modelId, predictionDataId: prediction.predictionDataId});

  let createData = {...prediction};
  delete createData.records;
  sampleCurlCommands.createPredictionCurlResponse = {
    metadata: {
      type: "DuplicateBugPrediction",
      async: {
        status: "in_progress"
      }
    },
    data: createData
  };

  sampleCurlCommands.getPredictionCurl = generateCurl(
    'GET',
    baseUrl,
    "api/duplicate/predictions/sample?include=records");

  sampleCurlCommands.getPredictionCurlResponse = {
    metadata: {
      type: "DuplicateBugPrediction",
      async: {
        status: "success"
      }
    },
    data: prediction
  };


  return sampleCurlCommands;
}

class DuplicateBugWizardPrediction extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      tabActiveKey: 0,
      curlCommands: generatePredictionCurlCommands(props.status.baseUrl)
    }
  }

  selectTab = (tabActiveKey) => {
    this.setState({tabActiveKey});
  };

  render() {
    const {tabActiveKey, curlCommands} = this.state;

    return (
      <div className="duplicate-bug-wizard-contents duplicate-bug-sample">
        <Tabs
          activeKey={tabActiveKey}
          onSelect={this.selectTab}
          animation={false}
          id="duplicateBugPredictionsTabs"
        >
          <Tab eventKey={0} title="Data">
            <div className="duplicate-bug-info-section">
              <h2>Predictions</h2>
              <p>Predictions are a list of likely duplicate bugs from the list of existing bugs.  For each record
              of prediction data (a new bug), a list of possible matches is generated from the existing bugs.</p>
              <JsonSample title="Sample Prediction Data Record" object={{
                id: "sample",
                title: "Bug report title",
                content: "Bug report main content"
              }}/>
              <JsonSample title="Matching Prediction Record" object={{
                0: {
                  id: "x.json",
                  title: "Highest Probability Duplicate Match"
                },
                1: {
                  id: "y.json",
                  title: "Likely Duplicate Match"
                },
                2: {
                  id: "z.json",
                  title: "Less Likely Duplicate Match"
                }
              }}/>
            </div>
          </Tab>
          <Tab eventKey={1} title="API">
            <div className="duplicate-bug-info-section">
              <h2>Using the API - Creating Predictions</h2>
              <p>Now that we have a data set and a model, we can create a prediction to see if there are any likely
              duplicates.  Since the job will take some time, the job will run asynchronously.  As it runs, we can
              poll the results until it is complete with async status of either <code>success</code> or <code>failure</code>
              </p>
              <h2>Create a Prediction</h2>
              <h3>Start the Prediction Job (Async)</h3>
              <Grid.Row>
                <Grid.Col className="sample-request-col" lg={6}>
                  <CodeSample title="Request" code={curlCommands.createPredictionCurl} language="bash"/>
                </Grid.Col>
                <Grid.Col className="sample-response-col" lg={6}>
                  <JsonSample title="Response" object={curlCommands.createPredictionCurlResponse}/>
                </Grid.Col>
              </Grid.Row>
              <h3>Poll the Async Creation of the Prediction</h3>
              <Grid.Row>
                <Grid.Col className="sample-request-col" lg={6}>
                  <CodeSample title="Request" code={curlCommands.getPredictionCurl} language="bash"/>
                </Grid.Col>
                <Grid.Col className="sample-response-col" lg={6}>
                  <JsonSample title="Response" object={curlCommands.getPredictionCurlResponse} showArrayLimit={10}/>
                </Grid.Col>
              </Grid.Row>
            </div>
          </Tab>
        </Tabs>
      </div>
    );
  };
}

function mapStateToProps(state) {
  return {
    ...state.duplicateBugReducer,
    ...state.statusReducer
  };
}

export default connect(mapStateToProps)(DuplicateBugWizardPrediction);
