import * as React from "react";

import RelatedBugsTableGrid from "./RelatedBugsTableGrid";


class PredictionRecordCard extends React.Component {

  render() {
    const {record} = this.props;
    const idText = record.id ?
      <span className="card-title-extra-info">{record.id}</span> :
      <span className="card-title-extra-info"><span className="spinner spinner-xs spinner-inline"/> Creating...</span>;

    let duplicateBugs = (
      <div className="duplicate-bug-demo-status alert alert-info">
        <span className="pficon pficon-spinner fa-spin"/> Duplicate Bug Analysis In Progress
      </div>);

    if (record.duplicateBugs) {
      duplicateBugs = <RelatedBugsTableGrid data={record.duplicateBugs}/>
    }
    return (
      <div className="data-record-card">
        <h3 className="card-title"><i className="fas fa-bug card-title-icon"/>Data Record</h3>
        <div className="attributes-container">
          <div className="field-label">ID:</div><div className="field-value">{idText}</div>
          <div className="field-label">Title:</div><div className="field-value">{record.title}</div>
          <div className="field-label">Contents:</div><div className="field-value">{record.content}</div>
        </div>
        <div className="field-label">Possible Duplicates:</div>
        <div className="related-bugs-container">
          {duplicateBugs}
        </div>
      </div>
    );
  }
}

export default PredictionRecordCard;
