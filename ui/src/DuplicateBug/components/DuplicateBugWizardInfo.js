import * as React from "react";
import { connect } from "react-redux";

class DuplicateBugWizardInfo extends React.Component {

  render() {
    const docUrl = "https://gitlab.com/opendatahub/ai-library/tree/master/duplicate_bug_detection";
    const {status} = this.props;
    const apiUrl = `${status.baseUrl}/api/duplicate`;

    return (
      <div className="duplicate-bug-wizard-contents duplicate-bug-info">
        <div className="duplicate-bug-info-section">
          <h2>What is Duplicate Bug Detection?</h2>
          <p>Duplicate bug detection is the ability to check if a newly reported software bug matches an existing
            software bug report.</p>
        </div>
        <div className="duplicate-bug-info-section">
          <h2>The API</h2>
          <p>Documentation: <a href={docUrl} target="_blank" rel="noopener noreferrer"><span>{docUrl}</span></a></p>
          <p>API Endpoint: <a href={apiUrl} target="_blank" rel="noopener noreferrer"><span>{apiUrl}</span></a></p>
        </div>
        <div className="duplicate-bug-info-section">
          <h2>Training Data</h2>
          <p>The AI Library includes the learning algorithm, but first we need to create a model using some training
            data.</p>
          <h2>Training a Model</h2>
          <p>The AI Library includes the duplicate bug detection learning algorithm, but first we need to create a model
            using some training data.</p>
          <h2>Prediction Data</h2>
          <p>After generating a trained model, we can use this model on a set of data and predict which test failures
            are unreliable and can be ignored.</p>
          <h2>Predictions</h2>
          <p>After generating a trained model, we can use this model on a set of data and predict which test failures
            are unreliable and can be ignored.</p>
          <h2>Demo</h2>
          <p>As a last step, we'll demonstrate a real prediction request and display the results and accompanying API
            requests</p>
        </div>
      </div>)
  };
}

function mapStateToProps(state) {
  return state.statusReducer;
}

export default connect(mapStateToProps)(DuplicateBugWizardInfo);
