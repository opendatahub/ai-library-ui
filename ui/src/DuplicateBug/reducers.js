import lodashGet from "lodash/get";

import {
  UPDATE_PREDICTION_UI,
  RESET_DUPLICATE_PREDICTION,
  CREATE_DUPLICATE_PREDICTION_DATA_PENDING,
  CREATE_DUPLICATE_PREDICTION_DATA_FULFILLED,
  CREATE_DUPLICATE_PREDICTION_DATA_RECORDS_PENDING,
  CREATE_DUPLICATE_PREDICTION_DATA_RECORDS_FULFILLED,
  GET_DUPLICATE_PREDICTION_PENDING,
  GET_DUPLICATE_PREDICTION_FULFILLED,
  GET_DUPLICATE_PREDICTION_REJECTED,
  CREATE_DUPLICATE_PREDICTION_PENDING,
  CREATE_DUPLICATE_PREDICTION_FULFILLED,
  CREATE_DUPLICATE_PREDICTION_REJECTED
} from "./actions";


const initialState = {
  ui: {
    browseTrainingData: false,
    bugFormTitle: "",
    bugFormContent: "",
    predictionDataTabActiveKey: 0,
    predictionTabActiveKey: 0
  },
  prediction: null,
  createPredictionDataRequest: null,
  createPredictionDataResponse: null,
  createPredictionDataRecordsRequest: null,
  createPredictionDataRecordsResponse: null,
  getPredictionResponseInProgress: null,
  getPredictionResponseComplete: null,
  getPredictionResponse: null,
  createPredictionRequest: null,
  createPredictionResponse: null,
  predictionAsyncStatus: null,
  predictionLoading: false,
  predictionError: null
};

export const duplicateBugReducer = (state = initialState, action) => {
  switch (action.type) {
    case UPDATE_PREDICTION_UI:
      return {
        ...state,
        ui: {...state.ui, ...action.payload.ui}
      };

    case RESET_DUPLICATE_PREDICTION:
      return initialState;

    case CREATE_DUPLICATE_PREDICTION_DATA_PENDING:
      return {
        ...state,
        createPredictionDataRequest: action.payload,
        createPredictionDataResponse: null
      };

    case CREATE_DUPLICATE_PREDICTION_DATA_FULFILLED:
      return {
        ...state,
        createPredictionDataResponse: lodashGet(action, "payload.response")
      };

    case CREATE_DUPLICATE_PREDICTION_DATA_RECORDS_PENDING:
      return {
        ...state,
        createPredictionDataRecordsRequest: action.payload,
        createPredictionDataRecordsResponse: null
      };

    case CREATE_DUPLICATE_PREDICTION_DATA_RECORDS_FULFILLED:
      return {
        ...state,
        createPredictionDataRecordsResponse: lodashGet(action, "payload.response")
      };

    case CREATE_DUPLICATE_PREDICTION_PENDING:
      return {
        ...state,
        prediction: null,
        predictionAsyncStatus: null,
        createPredictionRequest: lodashGet(action, "payload.body"),
        createPredictionResponse: null,
        predictionLoading: true,
        predictionError: null
      };
    case CREATE_DUPLICATE_PREDICTION_FULFILLED:
      return {
        ...state,
        prediction: lodashGet(action, "payload.response.data.data"),
        predictionAsyncStatus: lodashGet(action, "payload.response.data.metadata.async.status"),
        createPredictionResponse: lodashGet(action, "payload.response"),
        predictionLoading: false,
        predictionError: null
      };
    case CREATE_DUPLICATE_PREDICTION_REJECTED:
      return {
        ...state,
        prediction: null,
        predictionAsyncStatus: null,
        createPredictionResponse: null,
        predictionLoading: false,
        predictionError: lodashGet(action, "payload.error")
      };

    case GET_DUPLICATE_PREDICTION_PENDING:
      return {
        ...state,
        getPredictionDate: action.payload.requestDate,
        predictionLoading: true,
        predictionError: null
      };
    case GET_DUPLICATE_PREDICTION_FULFILLED:
      return duplicatePredictionFulfilledState(state, action);
    case GET_DUPLICATE_PREDICTION_REJECTED:
      console.error(action.payload.error);
      return {
        ...state,
        predictionLoading: false,
      };
    default:
      return state;
  }
};


function duplicatePredictionFulfilledState(state, action) {
  let getPredictionResponse = lodashGet(action, "payload.response");
  let predictionAsyncStatus = lodashGet(action, "payload.response.data.metadata.async.status");

  let newState = {
    ...state,
    prediction: lodashGet(action, "payload.response.data.data"),
    predictionAsyncStatus,
    getPredictionResponse,
    predictionLoading: false,
    predictionError: null
  };

  if (predictionAsyncStatus === "in_progress") {
    newState.getPredictionResponseInProgress = getPredictionResponse;
  } else {
    newState.getPredictionResponseComplete = getPredictionResponse;
  }

  return newState;
}