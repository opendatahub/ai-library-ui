import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter as Router } from "react-router-dom";
import { Provider } from "react-redux";
import configureStore from './configureStore'

import "../node_modules/patternfly/dist/css/patternfly.css";
import "../node_modules/patternfly/dist/css/patternfly-additions.css";
import "../node_modules/patternfly-react/dist/css/patternfly-react.css";
import "../node_modules/patternfly-react-extensions/dist/css/patternfly-react-extensions.css";
import "../node_modules/@fortawesome/fontawesome-free/css/all.css";
import "./index.css";
import App from "./App";

const store = configureStore();

ReactDOM.render(
  <Provider store={store}>
    <Router>
      <App/>
    </Router>
  </Provider>
  , document.getElementById('root'));

if (module.hot) {
  module.hot.accept('./App', () => {
    ReactDOM.render(
      <Provider store={store}>
        <Router>
          <App/>
        </Router>
      </Provider>,
      document.getElementById('root'),
    )
  })
}