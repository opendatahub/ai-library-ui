import axios from "axios"
import { createAxiosErrorNotification } from "../Notifications/actions";

export const GET_STATUS_PENDING = "GET_STATUS_PENDING";
export const getStatusPending = () => ({
  type: GET_STATUS_PENDING,
  payload: {}
});

export const GET_STATUS_FULFILLED = "GET_STATUS_FULFILLED";
export const getStatusFulfilled = (response) => ({
  type: GET_STATUS_FULFILLED,
  payload: {
    response
  }
});

export const GET_STATUS_REJECTED = "GET_STATUS_REJECTED";
export const getStatusRejected = (error) => ({
  type: GET_STATUS_REJECTED,
  payload: {
    error
  }
});

export const getStatus = () => {
  let url = "/api/status";
  return function (dispatch) {
    dispatch(getStatusPending());
    return axios.get(url)
      .then(response => dispatch(getStatusFulfilled(response)))
      .catch(error => {
        dispatch(createAxiosErrorNotification(error));
        dispatch(getStatusRejected(error));
      })
  }
};