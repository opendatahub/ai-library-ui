import * as React from "react";

import "./AxiosError.css"
import JsonSample from "../../JsonSample/components/JsonSample";

class AxiosError extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      showDetails: false
    }
  };

  render() {
    const {error} = this.props;
    const errorMessage = error.message ? <h2>{error.message}</h2> : <h2>Request Error Encountered</h2>
    const status = error.response ? <h3>Status Code {error.response.status}: {error.response.statusText}</h3> : "";

    return (
      <div className="AxiosError">
        {errorMessage}
        {status}
        <JsonSample title="Details" object={JSON.parse(JSON.stringify(error))} initHidden/>
      </div>
    )
  }
}

export default AxiosError;

