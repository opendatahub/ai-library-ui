import * as React from "react";
import { connect } from "react-redux";

import { Grid } from "patternfly-react";
import { generateSampleData } from "../utilities";
import CodeSample from "../../CodeSample/components/CodeSample"
import JsonSample from "../../JsonSample/components/JsonSample"


class SentimentWizardInfo extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      showRequest: true
    }
  }

  render() {
    const {status} = this.props;
    const docsUrl = "https://gitlab.com/opendatahub/ai-library/tree/master/actions/sentiment_analysis";
    const apiUrl = `${status.baseUrl}/api/sentiment`;
    const sampleData = generateSampleData(status.baseUrl);

    return (
      <div className="sentiment-wizard-contents sentiment-info">
        <div className="sentiment-info-section">
          <h2>What is Sentiment Analysis?</h2>
          <p>Sentiment Analysis refers to the use of several techniques to systematically identify, extract, quantify,
            and study emotional states and subjective information. It aims to determine the attitudes, opinions, or
            emotional reactions of a speaker with respect to some topic. It can often be helpful in ascertaining the
            sentiment of a product or brand when given conversation data such as a social media feed.</p>
        </div>
        <div className="sentiment-info-section">
          <h2>The API</h2>
          <p>Documentation: <a href={docsUrl} target="_blank" rel="noopener noreferrer"><span>{docsUrl}</span></a></p>
          <p>API Endpoint: <a href={apiUrl} target="_blank" rel="noopener noreferrer"><span>{apiUrl}</span></a></p>
          <p>By default, all sentiment analysis is done asynchronously. That means that doing a sentiment analysis
            will require doing both a creation of the sentiment analysis and then polling to fetch the result.</p>
        </div>
        <div className="sentiment-info-section">
          <h2>Create the Sentiment Analysis</h2>
          <p>Create the sentiment analysis and take note of the returned <code className="inline-code">id</code> and
            <code className="inline-code">status</code>.</p>
          <Grid.Row>
            <Grid.Col className="sample-request-col" lg={6}>
              <CodeSample title="Sample POST Request" code={sampleData.createTrainingDataCurl} language="bash"/>
            </Grid.Col>
            <Grid.Col className="sample-response-col" lg={6}>
              <JsonSample title="Sample POST Response" object={sampleData.postResponse}/>
            </Grid.Col>
          </Grid.Row>
        </div>
        <div className="sentiment-info-section">
          <h2>Get the Sentiment Analysis</h2>
          <p>Using a known <code className="inline-code">id</code>, you can then get the results. You can repeatedly
            request the results until the async metadata shows <code className="inline-code">"status": "success"</code>
            or in the case of errors, <code className="inline-code">"status": "failed"</code>
          </p>
          <Grid.Row>
            <Grid.Col className="sample-request-col" lg={6}>
              <CodeSample title="Sample GET Request" code={sampleData.getCurl} language="bash"/>
            </Grid.Col>
            <Grid.Col className="sample-response-col" lg={6}>
              <JsonSample title="Sample GET Response" object={sampleData.getResponse}/>
            </Grid.Col>
          </Grid.Row>
        </div>
      </div>)
  };
}

function mapStateToProps(state) {
  return state.statusReducer;
}

export default connect(mapStateToProps)(SentimentWizardInfo);

