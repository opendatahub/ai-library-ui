import * as React from "react";

import { Grid } from "patternfly-react";

const SentimentLegend = () => (
  <Grid.Row className="SentimentLegend">
    <Grid.Col sm={4} xsHidden className="sentiment-legend-labels">
      <div className="color-label">Color:</div>
      <div className="number-label">% Negative / % Positive:</div>
    </Grid.Col>
    <Grid.Col sm={8}>
      <div className="sentiment-legend-bar">
        <div className="very-negative-bar">Very Negative</div>
        <div className="negative-bar">Negative</div>
        <div className="neutral-bar">Neutral</div>
        <div className="positive-bar">Positive</div>
        <div className="very-positive-bar">Very Positive</div>
      </div>
      <div className="sentiment-legend-markers">
        <div className="very-negative-marker">-100%</div>
        <div className="spacer"/>
        <div className="negative-marker">-50%</div>
        <div className="spacer"/>
        <div className="neutral-marker">0</div>
        <div className="spacer"/>
        <div className="positive-marker">50%</div>
        <div className="spacer"/>
        <div className="very-positive-marker">100%</div>
      </div>
    </Grid.Col>
  </Grid.Row>
);

export default SentimentLegend;
