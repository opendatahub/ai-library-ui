import axios from "axios"
import qs from "qs";
import { createAxiosErrorNotification } from "../Notifications/actions";

export const GET_SENTIMENT_PENDING = "GET_SENTIMENT_PENDING";
export const getSentimentPending = () => ({
  type: GET_SENTIMENT_PENDING,
  payload: {}
});

export const GET_SENTIMENT_FULFILLED = "GET_SENTIMENT_FULFILLED";
export const getSentimentFulfilled = (response) => ({
  type: GET_SENTIMENT_FULFILLED,
  payload: {
    response
  }
});

export const GET_SENTIMENT_REJECTED = "GET_SENTIMENT_REJECTED";
export const getSentimentRejected = (error) => ({
  type: GET_SENTIMENT_REJECTED,
  payload: {
    error
  }
});

export const GET_SENTIMENT_IGNORED = "GET_SENTIMENT_IGNORED";
export const getSentimentIgnored = () => ({
  type: GET_SENTIMENT_IGNORED,
  payload: {}
});

export const getSentimentIgnoreNotFound = (id) => {
  let url = `/api/sentiment/${id}`;
  return async function (dispatch) {
    dispatch(getSentimentPending());
    try {
      let response = await axios.get(url);
      dispatch(getSentimentFulfilled(response));
    }
    catch (error) {
      if (!error.response || error.response.status !== 404) {
        dispatch(createAxiosErrorNotification(error));
        dispatch(getSentimentRejected(error));
      } else {
        dispatch(getSentimentIgnored());
      }
    }
  }
};

export const CREATE_SENTIMENT_PENDING = "CREATE_SENTIMENT_PENDING";
export const createSentimentPending = () => ({
  type: CREATE_SENTIMENT_PENDING,
  payload: {}
});

export const CREATE_SENTIMENT_FULFILLED = "CREATE_SENTIMENT_FULFILLED";
export const createSentimentFulfilled = (response) => ({
  type: CREATE_SENTIMENT_FULFILLED,
  payload: {
    response
  }
});

export const CREATE_SENTIMENT_REJECTED = "CREATE_SENTIMENT_REJECTED";
export const createSentimentRejected = (error) => ({
  type: CREATE_SENTIMENT_REJECTED,
  payload: {
    error
  }
});

export const createSentiment = (params, body) => {
  let paramStr = qs.stringify(params);
  paramStr = paramStr ? `?${paramStr}` : "";
  let url = `/api/sentiment${paramStr}`;
  return async function (dispatch) {
    dispatch(createSentimentPending());
    try {
      let response = await axios.post(url, body);
      dispatch(createSentimentFulfilled(response));
    }
    catch (error) {
      dispatch(createAxiosErrorNotification(error));
      dispatch(createSentimentRejected(error));
    }
  }
};

