
import React from "react";
import { TableGrid } from "patternfly-react-extensions";

const labelColSizes = {
  sm: 2
};
const logColSizes = {
  sm: 8
};
const flakeColSizes = {
  sm: 2
};

class PredictionResultsTableGrid extends React.Component {

  constructor(props) {
    super(props);

    let sortedItems = [];
    if (props.items) {
      sortedItems = props.items ? props.items.sort((a, b) =>  a.label.localeCompare(b.label)) : [];
    }

    this.state = {
      sortField: "label",
      items: sortedItems,
      isAscending: true,
      selectedItem: null,
      selectedField: null,
      selectedItems: []
    };
  }

  onSortToggle = id => {
    const { items, sortField, isAscending } = this.state;
    let updateAscending = true;

    if (id === sortField) {
      updateAscending = !isAscending;
    }

    items.sort((a, b) => {
      let compVal = 0;

      if (id === "log") {
        compVal = a.log.localeCompare(b.log);
      } else if (id === "flake") {
        compVal = a.flake - b.flake;
      }


      if (id === "label" || compVal === 0) {
        compVal = a.label.localeCompare(b.label);
      }

      if (!updateAscending) {
        compVal *= -1;
      }

      return compVal;
    });

    this.setState({ items, sortField: id, isAscending: updateAscending });
  };

  onSelect = (item, field) => {
    this.setState({ selectedItem: item, selectedField: field });
  };

  toggleSelection = item => {
    const { selectedItems } = this.state;
    let newSelections;
    const index = selectedItems.indexOf(item);

    if (index >= 0) {
      newSelections = [...selectedItems.slice(0, index), ...selectedItems.slice(index + 1)];
    } else {
      newSelections = [...selectedItems, item];
    }
    this.setState({ selectedItems: newSelections });
  };

  toggleAllSelections = () => {
    const { items, selectedItems} = this.state;
    this.setState({ selectedItems: selectedItems.length > 0 ? [] : [...items] });
  };

  renderItemRow = (item, index) => {
    const { selectType } = this.props;
    const { selectedItem, selectedField, selectedItems } = this.state;
    const selected = selectType === "checkbox" ? selectedItems.indexOf(item) >= 0 : selectedItem === item;
    const flakePercent = parseFloat(item.flake) * 100;
    return (
      <TableGrid.Row
        key={index}
        onClick={() => selectType === "row" && this.onSelect(item)}
        selected={(selectType === "row" || selectType === "checkbox") && selected}
        onToggleSelection={() => this.toggleSelection(item)}
      >
        <TableGrid.Col
          {...labelColSizes}
          onClick={() => selectType === "cell" && this.onSelect(item, "label")}
          selected={selectType === "cell" && selected && selectedField === "label"}
        >
          {item.label}
        </TableGrid.Col>
        <TableGrid.Col
          {...logColSizes}
          onClick={() => selectType === "cell" && this.onSelect(item, "log")}
          selected={selectType === "cell" && selected && selectedField === "log"}
        >
          {item.log}
        </TableGrid.Col>
        <TableGrid.Col
          {...flakeColSizes}
          onClick={() => selectType === "cell" && this.onSelect(item, "flake")}
          selected={selectType === "cell" && selected && selectedField === "flake"}
        >
          {flakePercent > 50
            ? <span className="flake-true">{flakePercent.toFixed(2)}%</span>
            : <span className="flake-false">{flakePercent.toFixed(2)}%</span>}
        </TableGrid.Col>
      </TableGrid.Row>
    );
  };

  render() {
    const { items, selectedItems, sortField, isAscending } = this.state;
    const { bordered, selectType } = this.props;
    return (
      <TableGrid className="FlakeTableGrid" bordered={bordered} selectType={selectType}>
        <TableGrid.Head
          showCheckbox={selectType === "checkbox"}
          allSelected={selectType === "checkbox" && selectedItems.length === items.length}
          partialSelected={selectType === "checkbox" && selectedItems.length > 0 && selectedItems.length < items.length}
          onToggleSelection={this.toggleAllSelections}
        >
          <TableGrid.ColumnHeader
            id="label"
            sortable
            isSorted={sortField === "label"}
            isAscending={isAscending}
            onSortToggle={() => this.onSortToggle("label")}
            {...labelColSizes}
          >
            Label
          </TableGrid.ColumnHeader>
          <TableGrid.ColumnHeader
            id="log"
            sortable
            isSorted={sortField === "log"}
            isAscending={isAscending}
            onSortToggle={() => this.onSortToggle("log")}
            {...logColSizes}
          >
            Log
          </TableGrid.ColumnHeader>
          <TableGrid.ColumnHeader
            id="flake"
            sortable
            isSorted={sortField === "flake"}
            isAscending={isAscending}
            onSortToggle={() => this.onSortToggle("flake")}
            {...flakeColSizes}
          >
            Flake
          </TableGrid.ColumnHeader>
        </TableGrid.Head>
        <TableGrid.Body>{items.map((item, index) => this.renderItemRow(item, index))}</TableGrid.Body>
      </TableGrid>
    );
  }
}

export default PredictionResultsTableGrid;