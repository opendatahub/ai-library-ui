import {} from "./actions";

const initialState = {};

export const homeReducer = (state = initialState, action) => {
  switch (action.type) {
    default:
      return state;
  }
};
