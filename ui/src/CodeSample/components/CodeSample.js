import * as React from "react";

import { Icon } from "patternfly-react"

import "./CodeSample.css"
import { CopyToClipboard } from "react-copy-to-clipboard";
import SyntaxHighlighter from "react-syntax-highlighter";
import ghg from "react-syntax-highlighter/styles/hljs/github-gist";

class CodeSample extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      showCode: true
    }
  };

  toggleDetails = () => {
    this.setState({showCode: !this.state.showCode})
  };

  render() {
    const {title, code, language} = this.props;
    const { showCode } = this.state;

    const toggleDetailsText = showCode
      ? <span><Icon type="fa" name="chevron-down"/> Hide</span>
      : <span><Icon type="fa" name="chevron-right"/> Show</span>;
    const codeSection = showCode ?
      <SyntaxHighlighter language={language} style={ghg}>{code}</SyntaxHighlighter>
      : "";

    return (
      <div className="CodeSample code-sample-section">
        <div className="code-sample-heading">
          <h4 className="code-sample-title">{title}</h4>
          <CopyToClipboard
            text={code}>
            <a className="clickable-empty-link code-sample-action"><Icon type="fa" name="copy"/> Copy</a>
          </CopyToClipboard>
          <a className="clickable-empty-link code-sample-action" onClick={this.toggleDetails}>{toggleDetailsText}</a>
        </div>
        {codeSection}
      </div>
    )
  }
}

export default CodeSample;

