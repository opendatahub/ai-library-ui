import urljoin from "url-join";

function generateCurl(method, baseUrl, subPath, body) {
  const url = urljoin(baseUrl, subPath );
  let curlCmd = `curl '${url}'`;

  if (method === 'POST') {
    curlCmd += ` \\
  -X POST \\
  -H 'Content-Type: application/json'`
  } else {
    curlCmd += ` \\
  -X ${method}`
  }

  curlCmd += ` \\
  -H 'Accept: application/json'`;

  if (body) {
    curlCmd += ` \\
  -d '${JSON.stringify(body)}'`
  }

  return curlCmd;
}

export default generateCurl;