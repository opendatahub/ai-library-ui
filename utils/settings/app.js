const getEnv = require("./utilities").getEnv;

const _init = () => {
  const baseUrl = getEnv("BASE_URL");

  // const sentiment = require("./sentiment");
  const aiLibrary = require("./aiLibrary");

  const status = {
    status: "OK",
    started: new Date(),
    baseUrl: baseUrl,
  };

  return {status, aiLibrary};
};

module.exports = _init();