const Wreck = require("wreck");
const urlJoin = require("url-join");

const proxyApiRequest = (request, h, serviceUrl, extraHeaders = {}) => {
  const apiRequestUrl = urlJoin(serviceUrl, request.params.params || "", request.url.search || "");
  let headers = {...request.headers, ...extraHeaders};
  delete headers.host;
  delete headers["content-length"];

  return Wreck.request(
    request.method,
    apiRequestUrl,
    {
      payload: request.payload,
      headers: headers,
      rejectUnauthorized: false
    });
};


module.exports.proxyApiRequest = proxyApiRequest;